package com.example.olgarudnytska.sklad.data.retrofit;

import java.net.ProtocolException;

public class ErrorException extends ProtocolException {
    public ErrorException() {}
    public ErrorException(String message) {
        super(message);
    }
}
