package com.example.olgarudnytska.sklad.presentation.view.productionCategory;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.data.response.category_description.Category;
import com.example.olgarudnytska.sklad.data.response.category_description.ProductInfo;
import com.example.olgarudnytska.sklad.di.Injector;
import com.example.olgarudnytska.sklad.presentation.adapters.ProductionCategoryAdapter;
import com.example.olgarudnytska.sklad.presentation.base.BaseFragment;
import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;
import com.example.olgarudnytska.sklad.presentation.view.main.MainActivity;
import com.example.olgarudnytska.sklad.presentation.view.productionProduct.ProductionProductFragment;
import com.example.olgarudnytska.sklad.tool.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ProductionCategoryFragment  extends BaseFragment implements IProductionCategoryView {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.header_category)
    TextView header;

    @BindView(R.id.text_products)
    TextView text_products;

    @BindView(R.id.count_items) TextView countItems;


    private ProductionCategoryPresenter presenter;
    private ProductionCategoryAdapter adapter;
    private Category category;
    private List<ProductInfo> productList;
    private int categoryId;
    private String categoryTitle;
    private int count;


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_production_category;
    }

    @Override
    protected void initView() {
        Bundle bundle = getArguments();
        String symbol = Injector.getPreferences().getLanguage();
        categoryId = bundle.getInt(Constants.CATEGORY_ID);
        categoryTitle = bundle.getString(Constants.CATEGORY_TITLE);
        count = bundle.getInt(Constants.CATEGORY_COUNT);
        countItems.setText(String.valueOf(count));
        if(symbol.equals("en")){
            if(count == 1){
                text_products.setText(getString(R.string.products_1)); }
            else {
                text_products.setText(getString(R.string.products_2)); }
        }
        else {
            String s = String.valueOf(count);
            if(s.endsWith("1")) {
                if (s.endsWith("11")){
                    text_products.setText(getString(R.string.products_3)); }
                else {
                    text_products.setText(getString(R.string.products_1)); }
            }
            else if(s.endsWith("2") || s.endsWith("3")|| s.endsWith("4")) {
                if (s.endsWith("12") || s.endsWith("13") || s.endsWith("14")) {
                    text_products.setText(getString(R.string.products_3)); }
                    else {
                    text_products.setText(getString(R.string.products_2)); }
            }

            else {
                text_products.setText(getString(R.string.products_3)); }
            }
        header.setText(categoryTitle);
        ((MainActivity) getActivity()).setArrowVisible();
        productList = new ArrayList<>();
        adapter = new ProductionCategoryAdapter(productList, listener);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        presenter.loadCategory(categoryId);
    }

    RecyclerListener listener = new RecyclerListener() {
        @Override
        public void onClick(RecyclerView.ViewHolder holder, View view, int position) {
            ProductInfo product = productList.get(position);
            startShowInfo(product);
        }
    };

    @Override
    protected void providePresenter() {

        presenter = new ProductionCategoryPresenter(Injector.getRepository());
        presenter.bindView(this);
    }

    @Override
    protected void unbindPresenter() {
        presenter.unbindView(this);
    }

    @Override
    public void setCategory(Category result) {
        if(result.getData().getProducts().size() > 0){
            productList.clear();
            productList.addAll(result.getData().getProducts());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showDialog(String s) {
        ((MainActivity) getActivity()).showDialog(s);
    }

    public void showNoConnectionDialog() {
        ((MainActivity) getActivity()).showNoConnectionDialog();
    }


    private void startShowInfo(ProductInfo product ){
        Fragment fragment = new ProductionProductFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.PRODUCT_ID, product.getProductId());
        bundle.putString(Constants.PRODUCT_TITLE, product.getTitle());
        bundle.putString(Constants.MAIN_IMAGE_PATH, product.getMainImagePath());
        String[] array = (product.getImagePaths()).toArray(new String[0]);
        bundle.putStringArray(Constants.IMAGES, array);
        ((MainActivity) getActivity()).showNewFragment(fragment, false, Constants.ANIM.RL,
                false, bundle);

    }

}
