package com.example.olgarudnytska.sklad.presentation.view.serviceSelect;
import com.example.olgarudnytska.sklad.SkladApp;
import com.example.olgarudnytska.sklad.data.repository.Repository;
import com.example.olgarudnytska.sklad.presentation.base.BasePresenter;

public class ServiceSelectPresenter extends BasePresenter<IServiceSelectView> {

    private Repository repository;

    public ServiceSelectPresenter(Repository repository) {
        this.repository = repository;

    }

    public void loadServices() {
        if(view().isActive()) {
            view().setResponse(SkladApp.getInstance().getServiceInfo());
        }
    }
}