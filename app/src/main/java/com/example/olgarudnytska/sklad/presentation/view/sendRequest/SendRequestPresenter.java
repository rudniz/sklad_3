package com.example.olgarudnytska.sklad.presentation.view.sendRequest;

import com.example.olgarudnytska.sklad.data.repository.Repository;
import com.example.olgarudnytska.sklad.data.request.SendOrderProduct;
import com.example.olgarudnytska.sklad.data.retrofit.NoConnectivityException;
import com.example.olgarudnytska.sklad.presentation.base.BasePresenter;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SendRequestPresenter extends BasePresenter<ISendRequestView> {

    private Repository repository;

    public SendRequestPresenter(Repository repository) {
        this.repository = repository;
    }

    public void sendRequest(SendOrderProduct product){
        view().showProgress();
        Subscription subscription = repository.sendOrder(product)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        view().setResponse(result);
                    }
                }, throwable -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        if(throwable instanceof NoConnectivityException){
                            view().showNoConnectionDialog();
                        }

                        else{
                            view().showDialog("Sending request  " + throwable.getMessage());
                        }
                    }
                });
        subscriptionsToUnbind.add(subscription);
    }

}
