
package com.example.olgarudnytska.sklad.data.response.product_description;

import com.example.olgarudnytska.sklad.data.response.category_description.ProductInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductData {

    @SerializedName("products")
    @Expose
    private ProductInfo products;

    public ProductInfo getProducts() {
        return products;
    }

    public void setProducts(ProductInfo products) {
        this.products = products;
    }

}
