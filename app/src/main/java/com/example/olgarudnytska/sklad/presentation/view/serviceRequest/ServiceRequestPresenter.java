package com.example.olgarudnytska.sklad.presentation.view.serviceRequest;

import com.example.olgarudnytska.sklad.data.repository.Repository;
import com.example.olgarudnytska.sklad.data.request.SendOrderService;
import com.example.olgarudnytska.sklad.data.retrofit.NoConnectivityException;
import com.example.olgarudnytska.sklad.presentation.base.BasePresenter;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ServiceRequestPresenter extends BasePresenter<IServiceRequestView> {

    private Repository repository;

    public ServiceRequestPresenter(Repository repository) {
        this.repository = repository;
    }

    public void sendServiceRequest(SendOrderService service){
        view().showProgress();
        Subscription subscription = repository.sendService(service)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        view().setResponse(result);
                    }
                }, throwable -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        if(throwable instanceof NoConnectivityException){
                            view().showNoConnectionDialog();
                        }

                        else{
                            view().showDialog("Sending service request  " + throwable.getMessage());
                        }
                    }
                });
        subscriptionsToUnbind.add(subscription);
    }

}