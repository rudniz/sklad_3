package com.example.olgarudnytska.sklad.presentation.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.olgarudnytska.sklad.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class EditCommentActivity extends AppCompatActivity {

    @BindView(R.id.editTextComment)
    EditText editTextComment;

    @BindView(R.id.btnAddComment) TextView btnAddComment;
    int GET_COMMENT = 1;
    TextWatcher textListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(s.length() > 1){
                btnAddComment.setEnabled(true);
                btnAddComment.getBackground().setTint(getResources().getColor(R.color.color_button));
            }
        }
    };
    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_comment);
        unbinder = ButterKnife.bind(this);
        if(getIntent().getStringExtra("COMMENT") != null){
            editTextComment.setText(getIntent().getStringExtra("COMMENT"));
        }
        editTextComment.requestFocus();
        btnAddComment.setEnabled(false);
        btnAddComment.getBackground().setTint(getResources().getColor(R.color.button_disabled ));
        editTextComment.addTextChangedListener(textListener);
    }

    @OnClick(R.id.btnAddComment)
    public void addComment(){
        if(editTextComment.getText().length() > 1){
            Intent intent=new Intent();
            intent.putExtra("COMMENT",editTextComment.getText().toString());
            setResult(GET_COMMENT,intent);
            finish();
        }
       else {
            btnAddComment.setEnabled(false);
            btnAddComment.getBackground().setTint(getResources().getColor(R.color.button_disabled ));
            Toast.makeText(this, getResources().getString(R.string.make_comment), Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.arrowLong)
    public void back(){
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
