
package com.example.olgarudnytska.sklad.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

public class CategoryInfo{

    @SerializedName("order")
    @Expose
    private int order;
    @SerializedName("categoryId")
    @Expose
    private int categoryId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("imagePath")
    @Expose
    private String imagePath;
    @SerializedName("count")
    @Expose
    private int count;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
