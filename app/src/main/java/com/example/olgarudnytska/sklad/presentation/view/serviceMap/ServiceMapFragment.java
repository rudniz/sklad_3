package com.example.olgarudnytska.sklad.presentation.view.serviceMap;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.SkladApp;
import com.example.olgarudnytska.sklad.data.response.service.Service;
import com.example.olgarudnytska.sklad.data.response.station.Station;
import com.example.olgarudnytska.sklad.data.response.station.StationInfo;
import com.example.olgarudnytska.sklad.di.Injector;
import com.example.olgarudnytska.sklad.presentation.base.BaseFragment;
import com.example.olgarudnytska.sklad.presentation.interface_models.MyListener;
import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;
import com.example.olgarudnytska.sklad.presentation.view.main.MainActivity;
import com.example.olgarudnytska.sklad.presentation.view.serviceSelect.ServiceSelectFragment;
import com.example.olgarudnytska.sklad.tool.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceMapFragment extends BaseFragment implements OnMapReadyCallback, IServiceMapView, GoogleMap.OnMarkerClickListener {

    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    @BindView(R.id.map)
    MapView mapView;
    private ServiceMapPresenter presenter;
    private GoogleMap map;
    private List<StationInfo> stationInfoList;
    private final int SERVICE_PRICE = 1;
    private final int LEAVE_REQUEST = 2;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_service_map;
    }

    @Override
    protected void initView() {
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }

        ((MainActivity) getActivity()).setLanguageSelectionInvisible();
        stationInfoList = new ArrayList<>();
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);
    }

    MyListener listener = new MyListener() {
        @Override
        public void onClick(int response) {
            switch (response){
                case SERVICE_PRICE:
                    presenter.loadServices();
            }
        }
    };

    @Override
    protected void providePresenter() {
        presenter = new ServiceMapPresenter(Injector.getRepository());
        presenter.bindView(this);

    }

    @Override
    protected void unbindPresenter() {
        presenter.unbindView(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mapView != null){
            Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
            if (mapViewBundle == null) {
                mapViewBundle = new Bundle();
                outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
            }
            mapView.onSaveInstanceState(mapViewBundle);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mapView != null){
            mapView.onStop();
        }
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if(mapView != null){
            mapView.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        UiSettings uiSettings = map.getUiSettings();
        uiSettings.setMapToolbarEnabled(true);
        uiSettings.setZoomControlsEnabled(true);
        LatLng curr = new LatLng(48.3794327, 31.1655807);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(curr, 7f));
        map.setOnMarkerClickListener(this);
        presenter.loadStations();

    }

    @Override
    public void setResponse(Station response) {
        if(response.getData().getStations() != null){
            stationInfoList.clear();
            stationInfoList.addAll(response.getData().getStations());
            for(StationInfo s: stationInfoList){
                LatLng markerLatLng = new LatLng
                        (Double.valueOf(s.getLatitude()), Double.valueOf(s.getLongitude()));
                MarkerOptions markerOption = new MarkerOptions()
                        .position(markerLatLng)
                        .title(s.getAddress())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
                Marker mark = map.addMarker(markerOption);
                mark.setTag(s);
            }
        }
    }

    @Override
    public void setServiceResponse(Service response) {
        if(response.getData().getServices().size() > 0){
            SkladApp.getInstance().setServiceList(response.getData().getServices());
        }
        Fragment fragment = new ServiceSelectFragment();
        ((MainActivity) getActivity()).showNewFragment(fragment,
                false, Constants.ANIM.RL, false, null);
    }


    @Override
    public void showDialog(String s) {
        ((MainActivity) getActivity()).showDialog(s);
    }

    public void showNoConnectionDialog() {
        ((MainActivity) getActivity()).showNoConnectionDialog();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        StationInfo s = (StationInfo) marker.getTag();
        ((MainActivity) getActivity()).showAddress(s, listener);
        return false;
    }

}
