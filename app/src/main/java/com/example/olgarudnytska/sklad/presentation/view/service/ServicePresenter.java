package com.example.olgarudnytska.sklad.presentation.view.service;

import android.util.Log;

import com.example.olgarudnytska.sklad.data.repository.Repository;
import com.example.olgarudnytska.sklad.presentation.base.BasePresenter;
import com.example.olgarudnytska.sklad.presentation.view.production.IProductionView;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ServicePresenter extends BasePresenter<IProductionView> {

    private Repository repository;

    public ServicePresenter(Repository repository) {
        this.repository = repository;
    }
}
