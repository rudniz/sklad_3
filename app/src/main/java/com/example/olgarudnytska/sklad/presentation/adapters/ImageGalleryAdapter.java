package com.example.olgarudnytska.sklad.presentation.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.presentation.interface_models.GlideApp;

import java.util.ArrayList;

public class ImageGalleryAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<String> imageUrls;
    public ImageGalleryAdapter(Context context, ArrayList<String> imageUrls) {
        this.context = context;
        this.imageUrls = imageUrls;
    }

    @Override
    public int getCount() {
        return imageUrls.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        GlideApp.with(context)
                .load(imageUrls.get(position))
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .into(imageView);
        container.addView((imageView));
        return imageView;
    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }
}