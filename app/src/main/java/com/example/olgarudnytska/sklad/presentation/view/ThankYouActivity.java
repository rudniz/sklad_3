package com.example.olgarudnytska.sklad.presentation.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.presentation.view.main.MainActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ThankYouActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnContinue)
    public void back(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
