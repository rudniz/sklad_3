package com.example.olgarudnytska.sklad.data.repository;

import com.example.olgarudnytska.sklad.data.request.SendOrderProduct;
import com.example.olgarudnytska.sklad.data.request.SendOrderService;
import com.example.olgarudnytska.sklad.data.response.ResultOk;
import com.example.olgarudnytska.sklad.data.response.category_description.Category;
import com.example.olgarudnytska.sklad.data.response.Catalog;
import com.example.olgarudnytska.sklad.data.response.product_description.Products;
import com.example.olgarudnytska.sklad.data.response.service.Service;
import com.example.olgarudnytska.sklad.data.response.station.Station;

import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface ServerApi {

    @GET("wp-json/rest/v1/categories")
    Observable <Catalog> loadCatalog();

    @GET("wp-json/rest/v1/products")
    Observable <Category> loadCategory(@Query("categoryId") int categoryId);

    @GET("wp-json/rest/v1/productDetails")
    Observable <Products> loadProduct(@Query("productId") int productId);

    @POST("ua/wp-json/rest/v1/buy")
    Observable<String> sendOrder(@Body SendOrderProduct body);

    @POST("ua/wp-json/rest/v1/bu")
    Observable<String> sendService(@Body SendOrderService body);

    @GET("wp-json/rest/v1/stations")
    Observable <Station> loadStations();

    @GET("wp-json/rest/v1/services")
    Observable <Service> loadServices();
}
