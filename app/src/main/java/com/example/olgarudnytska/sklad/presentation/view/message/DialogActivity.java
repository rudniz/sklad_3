package com.example.olgarudnytska.sklad.presentation.view.message;

import android.util.Log;
import android.widget.ImageView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.presentation.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class DialogActivity extends BaseActivity {


    @Override
    protected int getLayoutId() {
        return R.layout.progress_layout;
    }

    @Override
    protected void initView() {
    }


    @Override
    protected void providePresenter() {

    }

    @Override
    protected void unbindPresenter() {

    }
}
