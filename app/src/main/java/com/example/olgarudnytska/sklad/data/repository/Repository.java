package com.example.olgarudnytska.sklad.data.repository;

import com.example.olgarudnytska.sklad.data.preferences.PreferencesTools;
import com.example.olgarudnytska.sklad.data.request.SendOrderProduct;
import com.example.olgarudnytska.sklad.data.request.SendOrderService;
import com.example.olgarudnytska.sklad.data.response.ResultOk;
import com.example.olgarudnytska.sklad.data.response.category_description.Category;
import com.example.olgarudnytska.sklad.data.response.Catalog;
import com.example.olgarudnytska.sklad.data.response.product_description.Products;
import com.example.olgarudnytska.sklad.data.response.service.Service;
import com.example.olgarudnytska.sklad.data.response.station.Station;

import rx.Observable;

public class Repository {
    private PreferencesTools tools;
    private RemoteStorage remoteStorage;

    public Repository(PreferencesTools tools, RemoteStorage remoteStorage) {
        this.tools = tools;
        this.remoteStorage = remoteStorage;
    }

    public PreferencesTools getPreferences() {
        return tools;
    }

    public Observable<Catalog> loadCatalog() {
        return remoteStorage.loadCatalog();
    }
    public Observable<Category> loadCategory(int categoryId) {
        return remoteStorage.loadCategory(categoryId);
    }
    public Observable <Products> loadProduct(int productId){
        return remoteStorage.loadProduct(productId);
    }

    public Observable<String > sendOrder(SendOrderProduct body){
        return remoteStorage.sendOrder(body);
    }

    public Observable<String> sendService(SendOrderService body){
        return remoteStorage.sendService(body);
    }

    public Observable <Station> loadStations(){
        return remoteStorage.loadStations();
    }

    public Observable <Service> loadServices(){ return remoteStorage.loadServices();}

}
