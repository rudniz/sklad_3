package com.example.olgarudnytska.sklad.presentation.base;

public interface IBaseView {
    void showProgress();
    void hideProgress();
    boolean isActive();
}
