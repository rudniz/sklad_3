package com.example.olgarudnytska.sklad.presentation.view.serviceRequest;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.SkladApp;
import com.example.olgarudnytska.sklad.data.request.SendOrderService;
import com.example.olgarudnytska.sklad.data.response.service.ServiceInfo;
import com.example.olgarudnytska.sklad.di.Injector;
import com.example.olgarudnytska.sklad.presentation.adapters.ServiceRequestAdapter;
import com.example.olgarudnytska.sklad.presentation.base.BaseActivity;
import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;
import com.example.olgarudnytska.sklad.presentation.view.EditCommentActivity;
import com.example.olgarudnytska.sklad.presentation.view.ThankYouActivity;
import com.example.olgarudnytska.sklad.tool.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;

public class ServiceRequestActivity extends BaseActivity implements IServiceRequestView {

    int GET_COMMENT = 1;
    @BindView(R.id.textCheckConnection) TextView dialogMessage;
    @BindView(R.id.toolTitle) TextView toolTitle;
    @BindView(R.id.imageNoConnection) ImageView imageNoConnection;
    @BindView(R.id.plus) ImageView plus;
    @BindView(R.id.add_comment) TextView addComment;
    @BindView(R.id.commentText) TextView commentText;
    @BindView(R.id.textNoConnection) TextView dialogTitle;
    @BindView(R.id.layoutShowDialog) RelativeLayout layoutShowDialog;
    @BindView(R.id.rlSendingErrorBackground) RelativeLayout rlSendingErrorBackground;
    @BindView(R.id.rlComment) RelativeLayout commentLayout;
    @BindView(R.id.btnLeaveRequest) TextView btnLeaveRequest;
    @BindView(R.id.phone_code) TextView phoneCode;
    @BindView(R.id.name) EditText name;
    @BindView(R.id.phone_number) EditText phoneNumber;
    @BindView(R.id.recyclerServiceSend) RecyclerView recyclerView;
    @BindView(R.id.full_list) TextView fullList;
    @BindView(R.id.sum) TextView sum;
    @BindView(R.id.rlServiceDetails) RelativeLayout rlServiceDetails;

    private ServiceRequestAdapter adapter;
    private ServiceRequestPresenter presenter;
    private String currentComment = "";
    private List<ServiceInfo> selectedServiceInfoList;
    private List<ServiceInfo> fullServiceInfoList;
    private int total;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_service_request;
    }

    @Override
    protected void initView() {
        setTitle(getString(R.string.leave_request));
        name.requestFocus();
        setServiceInfo();
        adapter = new ServiceRequestAdapter(selectedServiceInfoList, listener);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        btnLeaveRequest.setEnabled(false);
        btnLeaveRequest.getBackground().setTint(getResources().getColor(R.color.button_disabled ));
        name.addTextChangedListener(textListener);
        phoneNumber.addTextChangedListener(textListener);
    }

    TextWatcher textListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(validate()){
                btnLeaveRequest.setEnabled(true);
                btnLeaveRequest.getBackground().setTint(getResources().getColor(R.color.color_button));
            }
            else {
                btnLeaveRequest.setEnabled(false);
                btnLeaveRequest.getBackground().setTint(getResources().getColor(R.color.button_disabled));
            }
        }
    };


    RecyclerListener listener = new RecyclerListener() {
        @Override
        public void onClick(RecyclerView.ViewHolder holder, View view, int position) {
            updateTotal();
        }
    };
    private boolean isShowingFullList = false;

    private void hideText(){
        rlServiceDetails.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }

    private void setServiceInfo(){
        selectedServiceInfoList = new ArrayList<>();
        fullServiceInfoList = new ArrayList<>();
        if(SkladApp.getInstance().getServiceInfo().size() > 0){
            fullServiceInfoList.clear();
            fullServiceInfoList.addAll(SkladApp.getInstance().getServiceInfo());
            for(ServiceInfo s: fullServiceInfoList){
                if(s.getCount() > 0){
                    selectedServiceInfoList.add(s);
                    total += s.getPrice() * s.getCount(); }
            }
            updateUi(); }
    }

    private void updateUi(){
        sum.setText(String.valueOf(total));
        if(selectedServiceInfoList.size() == 0){
            hideText();
            return;
        }
        if(selectedServiceInfoList.size() < 3){
            fullList.setVisibility(View.GONE);
        }
    }

    private boolean validate(){
        return (name.getText().length() > Constants.MINIMUM_NAME_LENGHT && isValidName(name.getText().toString())
                && phoneNumber.getText().length() > Constants.MINIMUM_PHONE_LENGHT);
    }

    public boolean isValidName(String string) {
        final String NAME_PATTERN = "^[\\p{L} .'-]+$";
        Pattern pattern = Pattern.compile(NAME_PATTERN);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    @Override
    protected void providePresenter() {
        presenter = new ServiceRequestPresenter(Injector.getRepository());
        presenter.bindView(this);
    }

    @Override
    protected void unbindPresenter() {
        presenter.unbindView(this);
    }

    public void showDialog(String s){
        layoutShowDialog.setVisibility(View.VISIBLE);
        imageNoConnection.setVisibility(View.GONE);
        dialogMessage.setText(s);
        dialogTitle.setText(getString(R.string.error));
    }

    public void showNoConnectionDialog(){
        layoutShowDialog.setVisibility(View.VISIBLE);
        dialogMessage.setText(R.string.check_connection);
        dialogTitle.setText(R.string.no_inet);
        imageNoConnection.setVisibility(View.VISIBLE);
    }

    @OnClick({R.id.layoutShowDialog, R.id.btnClose})
    public void hideDialod(){
        layoutShowDialog.setVisibility(View.GONE);
    }

    @OnClick({R.id.layoutShowDialogWhite, R.id.rlSendingError})
    public void doNothing(){
    }

    @OnClick({R.id.rlSendingErrorBackground, R.id.btnCancel})
    public void hideSendingError(){
        rlSendingErrorBackground.setVisibility(View.GONE);
    }

    @OnClick(R.id.arrowLong)
    public void back(){
        onBackPressed();
    }

    public void setTitle(String title) {
        toolTitle.setText(title);
    }


    @Override
    public void setResponse(String response) {
        if(response.equals("success")){
            startActivity(new Intent(this, ThankYouActivity.class));
            finish(); }
    }

    private void updateTotal() {
        total = 0;
        for(ServiceInfo s: selectedServiceInfoList){
            total += s.getPrice() * s.getCount(); }
        updateUi();
    }

    @OnClick(R.id.clear)
    public void clearList(){
        for(ServiceInfo s: selectedServiceInfoList){
            s.setCount(0); }
        selectedServiceInfoList.clear();
        adapter.notifyDataSetChanged();
        hideText();
    }




    @OnClick({R.id.plus, R.id.add_comment})
    public void getNewComment(){
        getComment(null);
    }


    public void getComment(String s){
        Intent getComment = new Intent(this, EditCommentActivity.class);
        if(s != null){
            getComment.putExtra("COMMENT", s);
        }
        startActivityForResult(getComment, GET_COMMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GET_COMMENT){
            if(data != null){
                addComment.setVisibility(View.INVISIBLE);
                plus.setVisibility(View.INVISIBLE);
                commentLayout.setVisibility(View.VISIBLE);
                try{
                    currentComment = data.getStringExtra("COMMENT");
                }catch (NullPointerException e){
                    currentComment = "";
                }
                commentText.setText(currentComment);
            }
        }
    }

    @OnClick(R.id.edit)
    public void editComment(){
        getComment(currentComment);
    }

    @OnClick(R.id.full_list)
    public void showFullList(){
        if(!isShowingFullList){
            adapter.ifFullList = true;
            adapter.notifyDataSetChanged();
            fullList.setText(getString(R.string.show_less));
            isShowingFullList = true;
        }
        else {
            adapter.ifFullList = false;
            adapter.notifyDataSetChanged();
            fullList.setText(getString(R.string.full_list));
            isShowingFullList = false;
        }

    }


    @OnClick({R.id.btnLeaveRequest, R.id.btnTry})
    public void leaveRequest() {
        if(validate()){
            SendOrderService sendOrderService = new SendOrderService();
            sendOrderService.setServiceIdList(selectedServiceInfoList);
            sendOrderService.setName(name.getText().toString());
            sendOrderService.setPhone(phoneNumber.getText().toString());
            sendOrderService.setComment(commentText.getText().toString());
            presenter.sendServiceRequest(sendOrderService);
        }
    }
}