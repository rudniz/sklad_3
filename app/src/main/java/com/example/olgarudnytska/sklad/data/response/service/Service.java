
package com.example.olgarudnytska.sklad.data.response.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Service {

    @SerializedName("data")
    @Expose
    private ServiceData data;

    public ServiceData getData() {
        return data;
    }

    public void setData(ServiceData data) {
        this.data = data;
    }

}
