package com.example.olgarudnytska.sklad.presentation.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.SkladApp;
import com.example.olgarudnytska.sklad.data.response.service.ServiceInfo;
import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ServiceSelectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ServiceInfo> serviceInfoList;
    private RecyclerListener listener;


    public ServiceSelectAdapter(List<ServiceInfo> list, RecyclerListener listener) {
        this.serviceInfoList = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ServiceHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_service_select, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ServiceInfo serviceInfo = serviceInfoList.get(position);
        ServiceHolder sh = (ServiceHolder) holder;
        sh.title.setText(serviceInfo.getTitle());
        sh.itemPrice.setText(String.valueOf(serviceInfo.getPrice()));
        sh.count.setText(String.valueOf(serviceInfoList.get(position).getCount()));
    }

    @Override
    public int getItemCount() {
        return serviceInfoList.size();
    }

    public class ServiceHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.serviceName)
        TextView title;
        @BindView(R.id.tvPriceItem)
        TextView itemPrice;
        @BindView(R.id.btnMinus)
        ImageView minus;
        @BindView(R.id.btnPlus)
        ImageView plus;
        @BindView(R.id.count_services)
        EditText count;
        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    serviceInfoList.get(getAdapterPosition())
                            .setCount(Integer.valueOf(count.getText().toString()));
                } else {
                    serviceInfoList.get(getAdapterPosition())
                            .setCount(0);
                }
            }
        };

        public ServiceHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            count.addTextChangedListener(watcher);
        }

        @OnClick(R.id.btnPlus)
        public void addCount() {
            int pos = getAdapterPosition();
            int c = serviceInfoList.get(pos).getCount();
            c += 1;
            updateCount(c, pos);
        }

        @OnClick(R.id.btnMinus)
        public void subtractCount() {
            int pos = getAdapterPosition();
            int c = serviceInfoList.get(pos).getCount();
            c = (c <= 1) ? 0 : (c - 1);
            updateCount(c, pos);
        }

        private void updateCount(int c, int pos) {
            count.setText(String.valueOf(c));
            serviceInfoList.get(pos).setCount(c);
            notifyItemChanged(pos);
            listener.onClick(null, null, pos);
        }
    }
}