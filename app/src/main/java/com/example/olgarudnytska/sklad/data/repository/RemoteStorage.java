package com.example.olgarudnytska.sklad.data.repository;

import com.example.olgarudnytska.sklad.data.request.SendOrderProduct;
import com.example.olgarudnytska.sklad.data.request.SendOrderService;
import com.example.olgarudnytska.sklad.data.response.ResultOk;
import com.example.olgarudnytska.sklad.data.response.category_description.Category;
import com.example.olgarudnytska.sklad.data.response.Catalog;
import com.example.olgarudnytska.sklad.data.response.product_description.Products;
import com.example.olgarudnytska.sklad.data.response.service.Service;
import com.example.olgarudnytska.sklad.data.response.station.Station;

import rx.Observable;

public class RemoteStorage {
    private ServerApi serverApi;

    public RemoteStorage(ServerApi serverApi) {
        this.serverApi = serverApi;
    }

    public Observable<Catalog> loadCatalog() {
        return serverApi.loadCatalog();
    }

    public Observable<Category> loadCategory(int categoryId) {
        return serverApi.loadCategory(categoryId);
    }
    public Observable <Products> loadProduct(int productId){
        return serverApi.loadProduct(productId);
    }
    public Observable<String> sendOrder(SendOrderProduct body){
        return serverApi.sendOrder(body);
    }

    public Observable<String> sendService(SendOrderService body){
        return serverApi.sendService(body);
    }


    public Observable <Station> loadStations(){
        return serverApi.loadStations();
    }

    public Observable <Service> loadServices(){
        return serverApi.loadServices();
    }




}
