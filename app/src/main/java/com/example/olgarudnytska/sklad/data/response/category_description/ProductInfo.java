
package com.example.olgarudnytska.sklad.data.response.category_description;

import java.util.List;

import com.example.olgarudnytska.sklad.data.response.product_description.ProductDescription;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductInfo {

    @SerializedName("productId")
    @Expose
    private int productId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("mainImagePath")
    @Expose
    private String mainImagePath;
    @SerializedName("imagePaths")
    @Expose
    private List<String> imagePaths = null;
    @SerializedName("videoLink")
    @Expose
    private String videoLink;
    @SerializedName("description")
    @Expose
    private ProductDescription description;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMainImagePath() {
        return mainImagePath;
    }

    public void setMainImagePath(String mainImagePath) {
        this.mainImagePath = mainImagePath;
    }

    public List<String> getImagePaths() {
        return imagePaths;
    }

    public void setImagePaths(List<String> imagePaths) {
        this.imagePaths = imagePaths;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public ProductDescription getDescription() {
        return description;
    }

    public void setDescription(ProductDescription description) {
        this.description = description;
    }

}
