package com.example.olgarudnytska.sklad.presentation.view.productionProduct;

import android.util.Log;

import com.example.olgarudnytska.sklad.data.repository.Repository;
import com.example.olgarudnytska.sklad.data.retrofit.NoConnectivityException;
import com.example.olgarudnytska.sklad.presentation.base.BasePresenter;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ProductionProductPresenter extends BasePresenter<IProductionProductView> {

    private Repository repository;

    public ProductionProductPresenter(Repository repository) {
        this.repository = repository;
    }

    public void loadProduct(int productId) {
        view().showProgress();
        Subscription subscription = repository.loadProduct(productId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        view().setProduct(list);
                    }
                }, throwable -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        if(throwable instanceof NoConnectivityException){
                            view().showNoConnectionDialog();
                        }

                        else{
                            view().showDialog("loadProduct " + throwable.getMessage());
                        }
                    }
                });
        subscriptionsToUnbind.add(subscription);
    }
}