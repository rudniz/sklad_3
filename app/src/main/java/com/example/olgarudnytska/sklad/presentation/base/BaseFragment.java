package com.example.olgarudnytska.sklad.presentation.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment implements IBaseView{

    protected View parentLayout;
    protected Unbinder unbinder;
    public Bundle savedInstanceState;

    protected abstract int getLayoutId();
    protected abstract void initView();
    protected abstract void providePresenter();
    protected abstract void unbindPresenter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parentLayout = view;
        unbinder = ButterKnife.bind(this, view);
        providePresenter();
        initView();
    }

    @Override
    public void onDestroyView() {
        if(unbinder != null) {
            unbinder.unbind();
        }
        unbindPresenter();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        setUserVisibleHint(false);
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        setUserVisibleHint(true);
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
    }

    @Override
    public void showProgress() {
        if(getActivity() != null) {
            ((BaseActivity)getActivity()).showProgress();
        }
    }

    @Override
    public void hideProgress() {
        if(getActivity() != null) {
            ((BaseActivity)getActivity()).hideProgress();
        }
    }

    @Override
    public boolean isActive() {
        return getActivity() != null && isAdded() && !isDetached()
                && getView() != null && !isRemoving();
    }
}
