
package com.example.olgarudnytska.sklad.data.response.category_description;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("products")
    @Expose
    private List<ProductInfo> products = null;
    @SerializedName("current_page")
    @Expose
    private int currentPage;
    @SerializedName("total_items")
    @Expose
    private int totalItems;

    public List<ProductInfo> getProducts() {
        return products;
    }

    public void setProducts(List<ProductInfo> products) {
        this.products = products;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

}
