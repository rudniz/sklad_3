package com.example.olgarudnytska.sklad.presentation.view.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.SkladApp;
import com.example.olgarudnytska.sklad.data.Language;
import com.example.olgarudnytska.sklad.data.response.station.StationInfo;
import com.example.olgarudnytska.sklad.presentation.adapters.LanguageAdapter;
import com.example.olgarudnytska.sklad.presentation.base.BaseActivity;
import com.example.olgarudnytska.sklad.presentation.interface_models.MyListener;
import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;
import com.example.olgarudnytska.sklad.presentation.view.about.AboutFragment;
import com.example.olgarudnytska.sklad.presentation.view.production.ProductionFragment;
import com.example.olgarudnytska.sklad.presentation.view.service.ServiceFragment;
import com.example.olgarudnytska.sklad.presentation.view.serviceSelect.ServiceSelectFragment;
import com.example.olgarudnytska.sklad.tool.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.toolTitle) TextView toolTitle;
    @BindView(R.id.lang) TextView languageSymbol;
    @BindView(R.id.tv_check_connection) TextView dialogMessage;
    @BindView(R.id.imgNoConnection) ImageView imgNoConnection;
    @BindView(R.id.textNoConnection) TextView dialogTitle;
    @BindView(R.id.radioGroup) RadioGroup radioGroup;
    @BindView(R.id.arrow) ImageView arrow;
    @BindView(R.id.arrowLong) ImageView arrowLong;
    @BindView(R.id.ll) RelativeLayout languagePopUpBackground;
    @BindView(R.id.rlShowDialog) RelativeLayout layoutShowDialog;
    @BindView(R.id.rlAddress) RelativeLayout rlAddress;
    @BindView(R.id.rlAddressWhite) RelativeLayout rlAddressWhite;
    @BindView(R.id.langL) LinearLayout languageSelection;
    @BindView(R.id.pop_up_lang) RelativeLayout popLang;
    @BindView(R.id.recycler_language) RecyclerView recyclerLanguage;
    @BindView(R.id.serviceAddress) TextView serviceAddress;
    @BindView(R.id.servicePhone) TextView servicePhone;

    List<Language> languageList;
    private final int SERVICE_PRICE = 1;
    private LanguageAdapter adapter;
    private String selectedSymbol;
    private final int LEAVE_REQUEST = 2;
    private MyListener myListener;
    private final int PRODUCTION = 0;
    private final int SERVICE = 1;
    private final int ABOUT = 2;
    private final int NEWS = 3;
    private int currentStationId;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        selectedSymbol = preferencesTools.getLanguage();
        createLanguageList();
        setLanguageSelected(selectedSymbol);
        languageSymbol.setText(selectedSymbol);
        setLanguageSelectionVisible();
        adapter = new LanguageAdapter(languageList, recycleListener);
        recyclerLanguage.setAdapter(adapter);
        recyclerLanguage.setLayoutManager(new LinearLayoutManager(this));
        radioGroup.setOnCheckedChangeListener(listener);
        ((RadioButton)radioGroup.getChildAt(preferencesTools.getPosition())).setChecked(true);
    }


    private void createLanguageList(){
        languageList = new ArrayList<>();
        languageList.add(new Language("Русский", "ru", false));
        languageList.add(new Language("Українська", "uk", false));
        languageList.add(new Language("English", "en", false));
    }

    private void setLanguageSelected(String symbol) {

        for (Language languageInList : languageList) {
            if (languageInList.getSymbol().equals(symbol)) {
                languageInList.setSelected(true);
                selectedSymbol = symbol;
            }
            else {
                languageInList.setSelected(false);
            }
        }
    }

    @OnClick({R.id.ll})
    public void hideLanguageSelection(){
        languagePopUpBackground.setVisibility(View.GONE);
    }

    @OnClick({R.id.pop_up_lang, R.id.rlShowDialogWhite, R.id.rlAddressWhite})
    public void doNothing(){
    }

    @OnClick(R.id.btnApply)
    public void saveLanguageToPreferences(){
        preferencesTools.setLanguage(selectedSymbol);
        SkladApp.getInstance().setNewUrl(selectedSymbol);
        setLocale(selectedSymbol);
        languageSymbol.setText(selectedSymbol);
        languagePopUpBackground.setVisibility(View.GONE);
        recreate();
    }

    public void setLanguageSelectionVisible(){
        languageSelection.setVisibility(View.VISIBLE);
        arrowLong.setVisibility(View.INVISIBLE);
    }
    public void setLanguageSelectionInvisible(){
        languageSelection.setVisibility(View.GONE);
    }

    public void setArrowVisible(){
        languageSelection.setVisibility(View.INVISIBLE);
        arrowLong.setVisibility(View.VISIBLE);
    }

    public void showAddress(StationInfo s, MyListener listener){
        if(listener != null){
            myListener = listener;
            rlAddress.setVisibility(View.VISIBLE);
            serviceAddress.setText(s.getAddress());
            servicePhone.setText(s.getPhones());
            currentStationId = s.getStationId();
        }
    }

    @OnClick(R.id.btnLeaveServiceRequest)
    public void leaveServiceRequest(){

    }

    @OnClick(R.id.servicePrice)
    public void showServicePrice(){
        hideAddress();
        myListener.onClick(SERVICE_PRICE);
    }



    @OnClick(R.id.rlAddress)
    public void hideAddress(){
        rlAddress.setVisibility(View.GONE);
    }


    public void setTitle(String title) {
        toolTitle.setText(title);
    }


    public void showNewFragment(Fragment fragment, boolean clear) {
        showNewFragment(fragment, clear, null, false, null);
    }

    public void showNewFragment(Fragment fragment, Constants.ANIM anim) {
        showNewFragment(fragment, false, anim, false, null);
    }

    public void showNewFragment(Fragment fragment, boolean clear, Constants.ANIM anim, boolean add, Bundle arg) {
        if(clear){
            clearBackStack();
        }
        String fragmentTag = fragment.getClass().getCanonicalName();
        int i = fragmentTag.lastIndexOf('.');
        fragmentTag = fragmentTag.substring(i + 1);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (anim != null) {
            switch (anim) {
                case RL:
                    transaction.setCustomAnimations(R.anim.rl_in, R.anim.rl_out,
                            R.anim.lr_in, R.anim.lr_out);
                    break;
                case LR :
                    transaction.setCustomAnimations(R.anim.lr_in, R.anim.lr_out,
                            R.anim.rl_in, R.anim.rl_out);
                    break;
                case TB :
                    transaction.setCustomAnimations(R.anim.tb_in, R.anim.tb_out,
                            R.anim.bt_in, R.anim.bt_out);
                    break;
                case BT :
                    transaction.setCustomAnimations(R.anim.bt_in, R.anim.bt_out,
                            R.anim.tb_in, R.anim.tb_out);
                    break;
            }
        }
        if (arg != null) {
            fragment.setArguments(arg);
        }
        if (add) {
            transaction.add(R.id.container, fragment, fragmentTag);
        } else {
            transaction.replace(R.id.container, fragment, fragmentTag);
        }
        transaction.addToBackStack(fragmentTag);
        transaction.commit();

    }

    public void clearBackStack() {
            FragmentManager manager = getSupportFragmentManager();
            if (manager.getBackStackEntryCount() > 0) {
                    FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
                    manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
    }

    RadioGroup.OnCheckedChangeListener listener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

            switch (checkedId){
                case R.id.btnProduction:
                    preferencesTools.setPosition(PRODUCTION);
                    showNewFragment(new ProductionFragment(), true);
                    break;
                case R.id.btnService:
                    preferencesTools.setPosition(SERVICE);
                    setTitle(R.string.service);
                    toolTitle.setText(R.string.service);
                    showNewFragment(new ServiceFragment(), true);
                    break;
                case R.id.btnAbout:
                    preferencesTools.setPosition(ABOUT);
                    setTitle(R.string.about);
                    toolTitle.setText(R.string.about);
                    showNewFragment(new AboutFragment(), true);
                    break;
                case R.id.btnNews:
                    preferencesTools.setPosition(NEWS);
                    setTitle(R.string.news);
                    toolTitle.setText(R.string.news);
                    break;
            }
        }
    };

    RecyclerListener recycleListener = new RecyclerListener() {
        @Override
        public void onClick(RecyclerView.ViewHolder holder, View view, int position) {
            Language language = languageList.get(position);
            selectedSymbol = language.getSymbol();
            setLanguageSelected(selectedSymbol);
            adapter.notifyDataSetChanged();
        }
    };


    @OnClick(R.id.langL)
    public void showLanguageSelection() {
        setLanguageSelected(preferencesTools.getLanguage());
        adapter.notifyDataSetChanged();
        languagePopUpBackground.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.arrowLong)
    public void back(){
        onBackPressed();
    }

    @OnClick({R.id.btnOk, R.id.rlShowDialog})
    public void closeDialog(){
        layoutShowDialog.setVisibility(View.GONE);
    }

    public void showDialog(String s){
        layoutShowDialog.setVisibility(View.VISIBLE);
        imgNoConnection.setVisibility(View.GONE);
        dialogMessage.setText(s);
        dialogTitle.setText(getString(R.string.error));
    }


    public void showNoConnectionDialog(){
        layoutShowDialog.setVisibility(View.VISIBLE);
        dialogMessage.setText(R.string.check_connection);
        dialogTitle.setText(R.string.no_inet);
        imgNoConnection.setVisibility(View.VISIBLE);
    }


    @Override
    protected void providePresenter() {
    }

    @Override
    protected void unbindPresenter() {
    }
}
