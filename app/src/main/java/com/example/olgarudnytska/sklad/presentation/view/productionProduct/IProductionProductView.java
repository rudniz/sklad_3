package com.example.olgarudnytska.sklad.presentation.view.productionProduct;

import com.example.olgarudnytska.sklad.data.response.product_description.Products;
import com.example.olgarudnytska.sklad.presentation.base.IBaseView;

public interface IProductionProductView extends IBaseView {
    void setProduct(Products result);
    void showDialog(String s);
    void showNoConnectionDialog();

}