package com.example.olgarudnytska.sklad;

import android.app.Application;
import android.util.Log;

import com.example.olgarudnytska.sklad.data.preferences.PreferencesTools;
import com.example.olgarudnytska.sklad.data.response.CategoryInfo;
import com.example.olgarudnytska.sklad.data.response.service.ServiceInfo;
import com.example.olgarudnytska.sklad.data.retrofit.ServerApiFactory;
import com.example.olgarudnytska.sklad.di.Injector;

import java.util.ArrayList;
import java.util.List;

public class SkladApp extends Application {
    private static SkladApp instance;
    private List<CategoryInfo> catalogList;
    private List<ServiceInfo> serviceList;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Injector.initInjector(this);
        catalogList = new ArrayList<>();
        serviceList = new ArrayList<>();

    }

    public void setNewUrl(String newLang) {
        if (newLang.equals("ru")) {
            ServerApiFactory.BASE_URL = ServerApiFactory.PART_URL;
        } else {
            ServerApiFactory.BASE_URL = ServerApiFactory.PART_URL + newLang + "/";
        }
        Injector.initInjector(this);
    }

    public static SkladApp getInstance() {
        return instance;
    }

    public List<CategoryInfo> getCatalogList(){
        return catalogList;
    }

    public void setCatalogList(List<CategoryInfo> catalog){
        catalogList.clear();
        catalogList.addAll(catalog);
    }

    public List<ServiceInfo> getServiceInfo(){
        return serviceList;
    }


    public void setServiceList(List<ServiceInfo> catalog){
//        if(serviceList.size() == 0) {
            serviceList.clear();
            serviceList.addAll(catalog);
//        }
    }
}
