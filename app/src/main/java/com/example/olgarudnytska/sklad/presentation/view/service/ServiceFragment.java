package com.example.olgarudnytska.sklad.presentation.view.service;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.presentation.base.BaseFragment;
import com.example.olgarudnytska.sklad.presentation.view.main.MainActivity;
//import com.example.olgarudnytska.sklad.presentation.view.serviceMap.ServiceMapFragment;
import com.example.olgarudnytska.sklad.presentation.view.serviceMap.ServiceMapFragment;
import com.example.olgarudnytska.sklad.tool.Constants;

import butterknife.BindView;

public class ServiceFragment extends BaseFragment{

    @BindView(R.id.btnChoose)
    TextView button;


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_service;
    }

    @Override
    protected void initView() {
        button.setOnClickListener(listener);
        ((MainActivity) getActivity()).setLanguageSelectionVisible();
    }

    @Override
    protected void providePresenter() {

    }

    @Override
    protected void unbindPresenter() {
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("QWERT", "starting a map");
            Fragment fragment = new ServiceMapFragment();
            ((MainActivity) getActivity()).showNewFragment(fragment, false, Constants.ANIM.RL, false, null);

            //start new Fragment
        }
    };


}
