package com.example.olgarudnytska.sklad.presentation.view.video;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.data.response.Catalog;
import com.example.olgarudnytska.sklad.tool.Constants;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoActivity extends YouTubeBaseActivity{

    private static final String API_KEY = Constants.API_KEY;
    YouTubePlayerView youTubePlayerView;
    String videoLink;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_video);
        String videoLink = getIntent().getStringExtra("videoLink");
        ButterKnife.bind(this);
        YouTubePlayerView youTubePlayerView =
                (YouTubePlayerView) findViewById(R.id.youtube);

        youTubePlayerView.initialize(API_KEY,
                new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                        YouTubePlayer youTubePlayer, boolean b) {

                        youTubePlayer.cueVideo(videoLink);
                    }

                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                        YouTubeInitializationResult youTubeInitializationResult) {

                    }
                });
    }

    @OnClick({R.id.circle_close, R.id.rlVideo})
    public void back(){
        onBackPressed();
    }
}
