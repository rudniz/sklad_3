package com.example.olgarudnytska.sklad.presentation.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.data.response.service.ServiceInfo;
import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ServiceRequestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ServiceInfo> serviceInfoList;
    private RecyclerListener listener;
    public boolean ifFullList = false;


    public ServiceRequestAdapter(List<ServiceInfo> list, RecyclerListener listener) {
        this.serviceInfoList = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ServiceHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_service_send, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ServiceInfo serviceInfo = serviceInfoList.get(position);
        ServiceHolder sh = (ServiceHolder) holder;
        sh.title.setText(serviceInfo.getTitle());
        sh.count.setText(String.valueOf(serviceInfoList.get(position).getCount()));
    }

    @Override
    public int getItemCount() {
        if(ifFullList){
            return serviceInfoList.size();
        }
        else{
            if(serviceInfoList.size() > 2){return 2;}
            else{return serviceInfoList.size();}
        }
    }

    public class ServiceHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.serviceTitle) TextView title;
        @BindView(R.id.count_item) TextView count;


        public ServiceHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.btnDelete)
        public void delete(){
            serviceInfoList.get(getAdapterPosition()).setCount(0);
            serviceInfoList.remove(getAdapterPosition());
            notifyDataSetChanged();
            listener.onClick(null, null, getAdapterPosition());
        }
    }
}
