package com.example.olgarudnytska.sklad.data.retrofit;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class BodyInterceptor implements Interceptor {

    public static final String CONNECTION_ENCODING = "UTF-8";

    private Gson gson;
    private String MESSAGE = "message";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        if (response.code() != 200) {
            String responseString = new String(response.body().bytes(), CONNECTION_ENCODING);
            JsonObject body = gson.fromJson(responseString, JsonObject.class);
            String st = body.get(MESSAGE).getAsString();
            if (st == null) {
                st = "Error internet " + response.code();
            }
            throw new ErrorException(st);
        } else {
            return response;
        }
    }

    public Gson getGson() {
        return gson;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }
}
