package com.example.olgarudnytska.sklad.presentation.view.productionCategory;

import android.util.Log;

import com.example.olgarudnytska.sklad.data.repository.Repository;
import com.example.olgarudnytska.sklad.data.retrofit.NoConnectivityException;
import com.example.olgarudnytska.sklad.presentation.base.BasePresenter;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ProductionCategoryPresenter extends BasePresenter<IProductionCategoryView> {

    private Repository repository;

    public ProductionCategoryPresenter(Repository repository) {
        this.repository = repository;
    }

    public void loadCategory(int categoryId) {
        Subscription subscription = repository.loadCategory(categoryId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        view().setCategory(list);
                    }
                }, throwable -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        if(throwable instanceof NoConnectivityException){
                            view().showNoConnectionDialog();
                        }

                        else{
                            view().showDialog("loadProduct " + throwable.getMessage());
                        }
                    }
                });
        subscriptionsToUnbind.add(subscription);
    }
}