package com.example.olgarudnytska.sklad.presentation.view.request;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.presentation.base.BaseFragment;

import butterknife.BindView;

public class ThankFragment extends BaseFragment {

    @BindView(R.id.btnContinue)
    TextView button;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_thank_you;
    }

    @Override
    protected void initView() {
        button.setOnClickListener(listener);
    }

    @Override
    protected void providePresenter() {
    }


    @Override
    protected void unbindPresenter() {
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("QWERT", "Thank you fragment: button Continue is clicked");
            //start new Fragment
        }
    };


}


