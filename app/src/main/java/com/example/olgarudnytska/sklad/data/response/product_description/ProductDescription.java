
package com.example.olgarudnytska.sklad.data.response.product_description;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDescription {

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("characteristics")
    @Expose
    private List<ProductCharacteristic> characteristics = null;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<ProductCharacteristic> getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(List<ProductCharacteristic> characteristics) {
        this.characteristics = characteristics;
    }

}
