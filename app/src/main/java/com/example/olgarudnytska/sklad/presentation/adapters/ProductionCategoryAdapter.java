package com.example.olgarudnytska.sklad.presentation.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.data.response.category_description.ProductInfo;
import com.example.olgarudnytska.sklad.presentation.interface_models.GlideApp;
import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductionCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ProductInfo> productList;
    private RecyclerListener listener;

    public ProductionCategoryAdapter(List<ProductInfo> list, RecyclerListener listener) {
        this.productList = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_production_category, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ProductInfo product = productList.get(position);
        ProductHolder ph = (ProductHolder) holder;
        ph.title.setText(product.getTitle());
        GlideApp.with(ph.itemView.getContext())
                .load(product.getMainImagePath())
                .placeholder(R.drawable.placeholder)
                .into(ph.image);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ProductHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.image)
        ImageView image;

        public ProductHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClick(ProductionCategoryAdapter.ProductHolder.this, v, getAdapterPosition());
                    }
                }
            });
        }
    }
}
