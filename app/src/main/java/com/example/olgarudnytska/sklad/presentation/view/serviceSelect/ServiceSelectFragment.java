package com.example.olgarudnytska.sklad.presentation.view.serviceSelect;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;

import com.example.olgarudnytska.sklad.SkladApp;
import com.example.olgarudnytska.sklad.data.response.service.Service;
import com.example.olgarudnytska.sklad.data.response.service.ServiceInfo;
import com.example.olgarudnytska.sklad.di.Injector;
import com.example.olgarudnytska.sklad.presentation.adapters.ServiceSelectAdapter;
import com.example.olgarudnytska.sklad.presentation.base.BaseFragment;
import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;
import com.example.olgarudnytska.sklad.presentation.view.main.MainActivity;
import com.example.olgarudnytska.sklad.presentation.view.serviceRequest.ServiceRequestActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ServiceSelectFragment extends BaseFragment implements IServiceSelectView {

    @BindView(R.id.recyclerService) RecyclerView recyclerView;
    @BindView(R.id.tvSum) TextView sum;


    private ServiceSelectPresenter presenter;
    private ServiceSelectAdapter adapter;
    private List<ServiceInfo> catalogList;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_service_select;
    }

     RecyclerListener listener = new RecyclerListener() {
        @Override
        public void onClick(RecyclerView.ViewHolder holder, View view, int position) {
            setTotal();
        }
    };

    @Override
    protected void initView() {
        ((MainActivity) getActivity()).setArrowVisible();
        ((MainActivity) getActivity()).setTitle(getString(R.string.service));
        catalogList = new ArrayList<>();
        adapter = new ServiceSelectAdapter(catalogList, listener);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.loadServices();
    }

    @Override
    protected void providePresenter() {
        presenter = new ServiceSelectPresenter(Injector.getRepository());
        presenter.bindView(this);
    }

    @Override
    protected void unbindPresenter() {
        presenter.unbindView(this);
    }

    @Override
    public void showDialog(String s) {
        ((MainActivity) getActivity()).showDialog(s);
    }

    public void showNoConnectionDialog() {
        ((MainActivity) getActivity()).showNoConnectionDialog();
    }

    private void setTotal(){
        int total = 0;
        for(ServiceInfo info: catalogList){
            total += info.getCount() * info.getPrice()+ 0.5;
        }
        sum.setText(String.valueOf(total));
    }


    public void setResponse(List<ServiceInfo> result) {
        if(result.size() > 0) {
            catalogList.clear();
            catalogList.addAll(result);
            setTotal();
            adapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.btnLeave)
    public void leaveServiceRequest(){
        Intent i = new Intent(getActivity(), ServiceRequestActivity.class);
        startActivity(i);
    }
}