package com.example.olgarudnytska.sklad.data.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.olgarudnytska.sklad.R;

public class PreferencesTools {
    private Context context;
    private static final String PREFERENCES_NAME = "PREFERENCES_NAME";
    private static final String TOKEN = "token";
    private static final String POSITION = "position";
    private static final String LANGUAGE = "language";

    public Context getContext(){
        return context;
    }


    public void setToken(String token) {
        getEditor(context).putString(TOKEN, token).commit();
    }

    public String getToken() {
        return getSharedPreferences(context).getString(TOKEN, "");
    }

    public void setPosition(int position) {
        getEditor(context).putInt(POSITION, position).commit();
    }

    public int getPosition() {
        return getSharedPreferences(context).getInt(POSITION, 0);
    }

    public void setLanguage(String language) {
        getEditor(context).putString(LANGUAGE, language).commit();

    }

    public String getLanguage() {
        return getSharedPreferences(context).getString(LANGUAGE, "ru");
    }


    public PreferencesTools(Context context) {
        this.context = context;
    }

    private SharedPreferences.Editor getEditor(Context context) {
        return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }
}
