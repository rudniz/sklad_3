package com.example.olgarudnytska.sklad.data.retrofit;

import com.example.olgarudnytska.sklad.data.preferences.PreferencesTools;
import com.example.olgarudnytska.sklad.data.repository.ServerApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServerApiFactory {

        public static String BASE_URL = "http://sms-skladtehnika.com/";
        public static final String PART_URL = "http://sms-skladtehnika.com/";

        private static ServerApi jsonApi;
        private static Gson gsonInstance;

        public static ServerApi getJsonApi(PreferencesTools preferencesTools) {
            if (preferencesTools.getLanguage().equals("ru")) {
                ServerApiFactory.BASE_URL = ServerApiFactory.PART_URL;
            } else {
                ServerApiFactory.BASE_URL = ServerApiFactory.PART_URL + preferencesTools.getLanguage() + "/";
            }
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            HeaderInterceptor headerInterceptor = new HeaderInterceptor(preferencesTools);
            BodyInterceptor bodyInterceptor = new BodyInterceptor();
            ConnectivityInterceptor connectivityInterceptor = new ConnectivityInterceptor(preferencesTools);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(connectivityInterceptor)
                    .addInterceptor(logging)
                    .addNetworkInterceptor(headerInterceptor)
                    .addNetworkInterceptor(bodyInterceptor)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .readTimeout(120, TimeUnit.SECONDS)
                    .followRedirects(true)
                    .followSslRedirects(true)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(buildGsonConverter())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(client)
                    .build();
            bodyInterceptor.setGson(gsonInstance);
            jsonApi = retrofit.create(ServerApi.class);

        return jsonApi;
    }

        public static Gson getGson() {
            return gsonInstance;
        }

        private static GsonConverterFactory buildGsonConverter() {
            GsonBuilder gsonBuilder = new GsonBuilder();

            gsonInstance = gsonBuilder.create();

            return GsonConverterFactory.create(gsonInstance);
        }
}
