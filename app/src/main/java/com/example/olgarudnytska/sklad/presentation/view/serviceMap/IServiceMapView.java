package com.example.olgarudnytska.sklad.presentation.view.serviceMap;

import com.example.olgarudnytska.sklad.data.response.service.Service;
import com.example.olgarudnytska.sklad.data.response.station.Station;
import com.example.olgarudnytska.sklad.presentation.base.IBaseView;

public interface IServiceMapView extends IBaseView {
    void setResponse(Station response);
    void setServiceResponse(Service response);

    void showDialog(String s);
    void showNoConnectionDialog();
}
