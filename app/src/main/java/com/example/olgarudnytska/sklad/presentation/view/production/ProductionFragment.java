package com.example.olgarudnytska.sklad.presentation.view.production;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.SkladApp;
import com.example.olgarudnytska.sklad.data.response.Catalog;
import com.example.olgarudnytska.sklad.data.response.CategoryInfo;
import com.example.olgarudnytska.sklad.data.response.CategoryInfoSorter;
import com.example.olgarudnytska.sklad.di.Injector;
import com.example.olgarudnytska.sklad.presentation.adapters.ProductionAdapter;
import com.example.olgarudnytska.sklad.presentation.base.BaseFragment;
import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;
import com.example.olgarudnytska.sklad.presentation.view.main.MainActivity;
import com.example.olgarudnytska.sklad.presentation.view.productionCategory.ProductionCategoryFragment;
import com.example.olgarudnytska.sklad.tool.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;

public class ProductionFragment extends BaseFragment implements IProductionView{

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    private ProductionPresenter presenter;
    private ProductionAdapter adapter;
    private List<CategoryInfo> catalogList;


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_production;
    }

    @Override
    protected void initView() {
        if (((MainActivity) getActivity()).countProgressStart > 0) {
            hideProgress();
        }
        ((MainActivity) getActivity()).setLanguageSelectionVisible();
        ((MainActivity) getActivity()).setTitle(getString(R.string.production));
        catalogList = new ArrayList<>();
        catalogList.addAll(SkladApp.getInstance().getCatalogList());
        adapter = new ProductionAdapter(catalogList, listener);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        presenter.loadCatalog();

    }


    RecyclerListener listener = new RecyclerListener() {
        @Override
        public void onClick(RecyclerView.ViewHolder holder, View view, int position) {
            CategoryInfo catalog = catalogList.get(position);
            startShowInfo(catalog);
        }
    };

    @Override
    protected void providePresenter() {
        presenter = new ProductionPresenter(Injector.getRepository());
        presenter.bindView(this);
    }

    @Override
    protected void unbindPresenter() {
        presenter.unbindView(this);
    }

    @Override
    public void showDialog(String s) {
        ((MainActivity) getActivity()).showDialog(s);
    }

    public void showNoConnectionDialog() {
        ((MainActivity) getActivity()).showNoConnectionDialog();
    }


    @Override
    public void setCatalog(Catalog result) {
        List<CategoryInfo> temp = result.getData().getCategories();
        if(temp.size() > 0){
            catalogList.clear();
            Collections.sort(temp, new CategoryInfoSorter());
            catalogList.addAll(temp);
            SkladApp.getInstance().setCatalogList(temp);
            adapter.notifyDataSetChanged();
        }
    }


    private void startShowInfo(CategoryInfo category ){
        Fragment fragment = new ProductionCategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.CATEGORY_ID, category.getCategoryId());
        bundle.putString(Constants.CATEGORY_TITLE, category.getTitle());
        bundle.putInt(Constants.CATEGORY_COUNT, category.getCount());
        ((MainActivity) getActivity()).showNewFragment(fragment, false, Constants.ANIM.RL, false, bundle);
        }
}
