package com.example.olgarudnytska.sklad.data.request;

import com.example.olgarudnytska.sklad.data.response.service.ServiceInfo;

import java.util.List;

public class SendOrderService {

    private String name;

    private String phone;

    private String countryCode;

    private List<ServiceInfo> serviceIdList;

    private int count;

    private String comment;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getCount() {
        return count; }

    public void setCount(int count) {
        this.count = count; }

    public List<ServiceInfo> getServiceIdList() {
        return serviceIdList; }

    public void setServiceIdList(List<ServiceInfo> serviceIdList) {
        this.serviceIdList = serviceIdList; }
}
