package com.example.olgarudnytska.sklad.presentation.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.data.response.product_description.ProductCharacteristic;
import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductionCharasteristicAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ProductCharacteristic> characteristics;
    private RecyclerListener listener;

    public ProductionCharasteristicAdapter(List<ProductCharacteristic> list, RecyclerListener listener) {
        this.characteristics = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CharacteristicHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_table, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ProductCharacteristic characteristic = characteristics.get(position);
        CharacteristicHolder ch = (CharacteristicHolder) holder;
        ch.title.setText(characteristic.getTitle());
        ch.description.setText(characteristic.getValue());
        if(position%2 == 0){
            ch.tableBackground.setBackgroundResource(R.color.grey_light_medium);
        }
        else {
            ch.tableBackground.setBackgroundResource(R.color.grey_light_table);
        }
    }

    @Override
    public int getItemCount() {
        return characteristics.size();
    }

    public class CharacteristicHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.table_title)
        TextView title;
        @BindView(R.id.table_description)
        TextView description;
        @BindView(R.id.llTable)
        LinearLayout tableBackground;

        public CharacteristicHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClick(ProductionCharasteristicAdapter
                                .CharacteristicHolder.this, v, getAdapterPosition());
                    }
                }
            });
        }
    }
}
