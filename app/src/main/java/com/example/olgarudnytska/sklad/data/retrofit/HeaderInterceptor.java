package com.example.olgarudnytska.sklad.data.retrofit;

import com.example.olgarudnytska.sklad.data.preferences.PreferencesTools;
import com.example.olgarudnytska.sklad.tool.Constants;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HeaderInterceptor implements Interceptor {
    private PreferencesTools preferencesTools;

    public HeaderInterceptor(PreferencesTools preferencesTools) {
        this.preferencesTools = preferencesTools;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest;
        newRequest = request.newBuilder()
                .addHeader(Constants.X_AUTH_TOKEN, preferencesTools.getToken())
                .build();
        return chain.proceed(newRequest);
    }
}
