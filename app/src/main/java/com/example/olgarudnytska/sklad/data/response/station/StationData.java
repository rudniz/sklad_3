
package com.example.olgarudnytska.sklad.data.response.station;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StationData {

    @SerializedName("stations")
    @Expose
    private List<StationInfo> stations = null;
    @SerializedName("current_page")
    @Expose
    private int currentPage;
    @SerializedName("total_items")
    @Expose
    private int totalItems;

    public List<StationInfo> getStations() {
        return stations;
    }

    public void setStations(List<StationInfo> stations) {
        this.stations = stations;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

}
