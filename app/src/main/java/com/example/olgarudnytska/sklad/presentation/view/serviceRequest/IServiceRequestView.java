package com.example.olgarudnytska.sklad.presentation.view.serviceRequest;

import com.example.olgarudnytska.sklad.presentation.base.IBaseView;

public interface IServiceRequestView extends IBaseView {
    void setResponse(String response);
    void showDialog(String s);
    void showNoConnectionDialog();
}
