package com.example.olgarudnytska.sklad.presentation.view.serviceMap;

import com.example.olgarudnytska.sklad.data.repository.Repository;
import com.example.olgarudnytska.sklad.data.retrofit.NoConnectivityException;
import com.example.olgarudnytska.sklad.presentation.base.BasePresenter;
import com.example.olgarudnytska.sklad.presentation.view.productionProduct.IProductionProductView;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ServiceMapPresenter extends BasePresenter<IServiceMapView> {

    private Repository repository;

    public ServiceMapPresenter(Repository repository) {
        this.repository = repository;
    }

    public void loadStations() {
        view().showProgress();
        Subscription subscription = repository.loadStations()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        view().setResponse(list);
                    }
                }, throwable -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        if(throwable instanceof NoConnectivityException){
                            view().showNoConnectionDialog();
                        }

                        else{
                            view().showDialog("Load stations " + throwable.getMessage());
                        }
                    }
                });
        subscriptionsToUnbind.add(subscription);
    }

    public void loadServices() {
        view().showProgress();
        Subscription subscription = repository.loadServices()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        view().setServiceResponse(list);
                    }
                }, throwable -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        if(throwable instanceof NoConnectivityException){
                            view().showNoConnectionDialog();
                        }

                        else{
                            view().showDialog("Load services " + throwable.getMessage());
                        }
                    }
                });
        subscriptionsToUnbind.add(subscription);
    }
}
