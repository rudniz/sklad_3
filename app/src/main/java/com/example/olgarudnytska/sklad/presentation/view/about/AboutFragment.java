package com.example.olgarudnytska.sklad.presentation.view.about;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.presentation.base.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

public class AboutFragment  extends BaseFragment {

    @BindView(R.id.btnWrite) TextView button;
    @BindView(R.id.full_description) TextView fullDescription;
    @BindView((R.id.description2)) TextView description2;

    private boolean isVisible = false;


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_about;
    }

    @Override
    protected void initView() {
        button.setOnClickListener(listener);
    }

    @Override
    protected void providePresenter() {

    }

    @OnClick(R.id.full_description)
    public void showDescription(){
        if(!isVisible){
            description2.setVisibility(View.VISIBLE);
            fullDescription.setText(R.string.show_less);
            isVisible = true;
        }

        else {
            isVisible = false;
            description2.setVisibility(View.GONE);
            fullDescription.setText(R.string.full_description);
        }

    }

    @OnClick(R.id.ll_video)
    public void showVideo(){
        Log.d("QWERT", "show video from About Fragment");
    }

    @Override
    protected void unbindPresenter() {
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("QWERT", "About fragment: button Contact is clicked");
            //start new Fragment
        }
    };


}

