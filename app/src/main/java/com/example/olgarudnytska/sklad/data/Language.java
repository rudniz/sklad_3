package com.example.olgarudnytska.sklad.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Language {
    private String language;
    private String symbol;
    private boolean isSelected;

    public Language(String language, String symbol, boolean isSelected){
        this.language = language;
        this.symbol = symbol;
        this.isSelected = isSelected;
    }


    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
