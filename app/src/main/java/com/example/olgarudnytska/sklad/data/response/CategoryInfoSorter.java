package com.example.olgarudnytska.sklad.data.response;

import java.util.Comparator;

public class CategoryInfoSorter implements Comparator<CategoryInfo> {

    public int compare(CategoryInfo one, CategoryInfo another){
        int returnVal = 0;

        if(one.getOrder() < another.getOrder()){
            returnVal =  -1;
        }else if(one.getOrder() > another.getOrder()){
            returnVal =  1;
        }else if(one.getOrder() == another.getOrder()){
            returnVal =  0;
        }
        return returnVal;
    }
}
