package com.example.olgarudnytska.sklad.presentation.view.sendRequest;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.data.request.SendOrderProduct;
import com.example.olgarudnytska.sklad.di.Injector;
import com.example.olgarudnytska.sklad.presentation.base.BaseActivity;
import com.example.olgarudnytska.sklad.presentation.interface_models.GlideApp;
import com.example.olgarudnytska.sklad.presentation.view.EditCommentActivity;
import com.example.olgarudnytska.sklad.presentation.view.ThankYouActivity;
import com.example.olgarudnytska.sklad.tool.Constants;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;

public class SendRequestActivity extends BaseActivity implements ISendRequestView {

    int GET_COMMENT = 1;
    @BindView(R.id.textCheckConnection) TextView dialogMessage;
    @BindView(R.id.toolTitle) TextView toolTitle;
    @BindView(R.id.imageNoConnection) ImageView imageNoConnection;
    @BindView(R.id.plus) ImageView plus;
    @BindView(R.id.add_comment) TextView addComment;
    @BindView(R.id.commentText) TextView commentText;
    @BindView(R.id.textNoConnection) TextView dialogTitle;
    @BindView(R.id.layoutShowDialog) RelativeLayout layoutShowDialog;
    @BindView(R.id.rlSendingErrorBackground) RelativeLayout rlSendingErrorBackground;
    @BindView(R.id.rlComment) RelativeLayout commentLayout;
    @BindView(R.id.btnLeaveRequest) TextView btnLeaveRequest;
    @BindView(R.id.phone_code) TextView phoneCode;
    @BindView(R.id.img_order) ImageView imgOrder;
    @BindView(R.id.product_title) TextView product_title;
    @BindView(R.id.name) EditText name;
    @BindView(R.id.phone_number) EditText phoneNumber;
    TextWatcher textListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(validate()){
                btnLeaveRequest.setEnabled(true);
                btnLeaveRequest.getBackground().setTint(getResources().getColor(R.color.color_button));
            }
        }
    };
    private String mainImagePath;
    private int productId;
    private String productTitle;
    private SendRequestPresenter presenter;
    private String currentComment = "";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_send_request;
    }

    @Override
    protected void initView() {
        Intent intent = getIntent();
        productId = intent.getIntExtra(Constants.PRODUCT_ID, 0);
        productTitle = intent.getStringExtra(Constants.PRODUCT_TITLE);
        mainImagePath = intent.getStringExtra(Constants.MAIN_IMAGE_PATH);
        product_title.setText(productTitle);
        setTitle(R.string.leave_request);
        name.requestFocus();
        GlideApp.with(getApplicationContext())
                .load(mainImagePath)
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .into(imgOrder);

        btnLeaveRequest.setEnabled(false);
        btnLeaveRequest.getBackground().setTint(getResources().getColor(R.color.button_disabled ));
        name.addTextChangedListener(textListener);
        phoneNumber.addTextChangedListener(textListener);
    }

    private boolean validate(){
        return (name.getText().length() > Constants.MINIMUM_NAME_LENGHT && isValidName(name.getText().toString())
        && phoneNumber.getText().length() > Constants.MINIMUM_PHONE_LENGHT);
    }

    public boolean isValidName(String string) {
        final String NAME_PATTERN = "^[\\p{L} .'-]+$";
        Pattern pattern = Pattern.compile(NAME_PATTERN);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    @Override
    protected void providePresenter() {
        presenter = new SendRequestPresenter(Injector.getRepository());
        presenter.bindView(this);
    }

    @Override
    protected void unbindPresenter() {
        presenter.unbindView(this);
    }

    public void showDialog(String s){
        layoutShowDialog.setVisibility(View.VISIBLE);
        imageNoConnection.setVisibility(View.GONE);
        dialogMessage.setText(s);
        dialogTitle.setText(getString(R.string.error));
    }

    public void showNoConnectionDialog(){
        layoutShowDialog.setVisibility(View.VISIBLE);
        dialogMessage.setText(R.string.check_connection);
        dialogTitle.setText(R.string.no_inet);
        imageNoConnection.setVisibility(View.VISIBLE);
    }

    @OnClick({R.id.layoutShowDialog, R.id.btnClose})
    public void hideDialod(){
        layoutShowDialog.setVisibility(View.GONE);
    }

    @OnClick({R.id.layoutShowDialogWhite, R.id.rlSendingError})
    public void doNothing(){
    }

    @OnClick({R.id.rlSendingErrorBackground, R.id.btnCancel})
    public void hideSendingError(){
        rlSendingErrorBackground.setVisibility(View.GONE);
    }

    @OnClick(R.id.arrowLong)
    public void back(){
        onBackPressed();
    }

    public void setTitle(String title) {
        toolTitle.setText(title);
    }


    @Override
    public void setResponse(String response) {
        if(response.equals("success")){
            startActivity(new Intent(this, ThankYouActivity.class));
            finish();
        }
    }


    @OnClick({R.id.plus, R.id.add_comment})
    public void getNewComment(){
        getComment(null);
    }


    public void getComment(String s){
        Intent getComment = new Intent(this, EditCommentActivity.class);
        if(s != null){
            getComment.putExtra("COMMENT", s);
        }
        startActivityForResult(getComment, GET_COMMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GET_COMMENT){
            if(data != null){
                addComment.setVisibility(View.GONE);
                plus.setVisibility(View.GONE);
                commentLayout.setVisibility(View.VISIBLE);
                try{
                    currentComment = data.getStringExtra("COMMENT");
                }catch (NullPointerException e){
                    currentComment = "";
                }
                commentText.setText(currentComment);
            }
        }
    }

    @OnClick(R.id.edit)
    public void editComment(){
        getComment(currentComment);
    }



    @OnClick({R.id.btnLeaveRequest, R.id.btnTry})
    public void leaveRequest() {
        if(validate()){
            SendOrderProduct sendOrderProduct = new SendOrderProduct();
            sendOrderProduct.setProductId(productId);
            sendOrderProduct.setName(name.getText().toString());
            sendOrderProduct.setPhone(phoneNumber.getText().toString());
            sendOrderProduct.setComment(commentText.getText().toString());
            presenter.sendRequest(sendOrderProduct);
            }
        }


}
