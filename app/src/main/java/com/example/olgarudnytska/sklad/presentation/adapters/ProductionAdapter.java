package com.example.olgarudnytska.sklad.presentation.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.data.response.CategoryInfo;
import com.example.olgarudnytska.sklad.presentation.interface_models.GlideApp;
import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProductionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CategoryInfo> items;
    private RecyclerListener listener;

    public ProductionAdapter(List<CategoryInfo> items, RecyclerListener listener) {
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CatalogHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_production, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CategoryInfo catalog = items.get(position);
        CatalogHolder ch = (CatalogHolder) holder;
        ch.title.setText(catalog.getTitle());
        ch.count.setText(String.valueOf(catalog.getCount()));
        GlideApp.with(ch.itemView.getContext())
                .load(catalog.getImagePath())
                .placeholder(R.drawable.placeholder)
                .into(ch.image);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class CatalogHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title) TextView title;
        @BindView(R.id.count) TextView count;
        @BindView(R.id.image) ImageView image;

        public CatalogHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClick(CatalogHolder.this, v, getAdapterPosition());
                    }
                }
            });
        }
    }
}
