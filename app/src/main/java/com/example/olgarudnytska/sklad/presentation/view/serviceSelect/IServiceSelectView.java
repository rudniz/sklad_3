package com.example.olgarudnytska.sklad.presentation.view.serviceSelect;

import com.example.olgarudnytska.sklad.data.response.service.Service;
import com.example.olgarudnytska.sklad.data.response.service.ServiceInfo;
import com.example.olgarudnytska.sklad.presentation.base.IBaseView;

import java.util.List;

public interface IServiceSelectView extends IBaseView {
    void setResponse(List<ServiceInfo> response);
    void showDialog(String s);
    void showNoConnectionDialog();
}