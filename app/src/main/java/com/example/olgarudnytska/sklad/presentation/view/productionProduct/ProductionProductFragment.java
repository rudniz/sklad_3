package com.example.olgarudnytska.sklad.presentation.view.productionProduct;

    import android.content.Intent;
    import android.os.Bundle;
    import android.support.v4.view.ViewPager;
    import android.support.v7.widget.LinearLayoutManager;
    import android.support.v7.widget.RecyclerView;
    import android.view.View;
    import android.widget.LinearLayout;
    import android.widget.TextView;

    import com.example.olgarudnytska.sklad.R;
    import com.example.olgarudnytska.sklad.data.response.category_description.ProductInfo;
    import com.example.olgarudnytska.sklad.data.response.product_description.ProductCharacteristic;
    import com.example.olgarudnytska.sklad.data.response.product_description.Products;
    import com.example.olgarudnytska.sklad.di.Injector;
    import com.example.olgarudnytska.sklad.presentation.adapters.ImageGalleryAdapter;
    import com.example.olgarudnytska.sklad.presentation.adapters.ProductionCharasteristicAdapter;
    import com.example.olgarudnytska.sklad.presentation.base.BaseFragment;
    import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;
    import com.example.olgarudnytska.sklad.presentation.view.main.MainActivity;
    import com.example.olgarudnytska.sklad.presentation.view.sendRequest.SendRequestActivity;
    import com.example.olgarudnytska.sklad.presentation.view.video.VideoActivity;
    import com.example.olgarudnytska.sklad.tool.Constants;
    import com.example.olgarudnytska.sklad.tool.PagerIndicator;

    import java.util.ArrayList;
    import java.util.Arrays;
    import java.util.List;

    import butterknife.BindView;
    import butterknife.OnClick;

public class ProductionProductFragment  extends BaseFragment implements IProductionProductView {

    @BindView(R.id.title_product) TextView header;
    @BindView(R.id.full_description) TextView full_description;

    @BindView(R.id.product_description_1) TextView product_description_1;
    @BindView(R.id.product_description_2) TextView product_description_2;
    @BindView(R.id.viewPager) ViewPager viewPager;
    @BindView(R.id.recycler_table) RecyclerView recycler_table;
    @BindView(R.id.llVideo) LinearLayout llVideo;
    @BindView(R.id.indicator) PagerIndicator indicator;


    private ProductionProductPresenter presenter;
    private ProductionCharasteristicAdapter adapter;
    private List<ProductCharacteristic> characteristicList;
    private String mainImagePath;
    private int productId;
    private String productTitle;
    private boolean descriptionIsActive = false;
    String videoLink;


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_production_product;
    }

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {
        }
        @Override
        public void onPageSelected(int i) {
            indicator.setSelect(i);
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

    @Override
    protected void initView() {
        Bundle bundle = getArguments();
        productId = bundle.getInt(Constants.PRODUCT_ID);
        productTitle = bundle.getString(Constants.PRODUCT_TITLE);
        mainImagePath = bundle.getString(Constants.MAIN_IMAGE_PATH);
        ArrayList<String> url = new ArrayList<>();
        List<String> arrayList = Arrays.asList(bundle.getStringArray(Constants.IMAGES));
        url.addAll(arrayList);
        indicator.setCount(url.size());
        header.setText(productTitle);
        ImageGalleryAdapter imageAdapter = new ImageGalleryAdapter(getContext(), url);
        viewPager.setAdapter(imageAdapter);
        characteristicList = new ArrayList<>();
        adapter = new ProductionCharasteristicAdapter(characteristicList, listener);
        recycler_table.setAdapter(adapter);
        recycler_table.setLayoutManager(new LinearLayoutManager(getActivity()));
        presenter.loadProduct(productId);
        viewPager.addOnPageChangeListener(pageChangeListener);
    }


    RecyclerListener listener = new RecyclerListener() {
        @Override
        public void onClick(RecyclerView.ViewHolder holder, View view, int position) {
            ProductCharacteristic characteristic = characteristicList.get(position);
            startShowInfo(characteristic);
        }
    };

    @Override
    protected void providePresenter() {

        presenter = new ProductionProductPresenter(Injector.getRepository());
        presenter.bindView(this);
    }

    @Override
    protected void unbindPresenter() {
        presenter.unbindView(this);
    }

    @OnClick({R.id.full_description})
    public void showFullDescripton(){
        if(!descriptionIsActive){
            descriptionIsActive = true;
            full_description.setText(R.string.show_less);
            product_description_2.setVisibility(View.VISIBLE);
        }
        else {
            descriptionIsActive = false;
            full_description.setText(R.string.full_description);
            product_description_2.setVisibility(View.GONE);
        }

    }

    @Override
    public void setProduct(Products result) {
        if(result.getData().getProducts() != null){
            ProductInfo currentProduct = result.getData().getProducts();
            if(!result.getData().getProducts().getVideoLink().equals("")){
                String temp = result.getData().getProducts().getVideoLink();
                int index = temp.lastIndexOf('/');
                videoLink = temp.substring(index + 1);
                llVideo.setVisibility(View.VISIBLE);

            }
            characteristicList.clear();
            characteristicList.addAll(currentProduct.getDescription().getCharacteristics());
            String text = currentProduct.getDescription().getText();
            if(text.length() > Constants.SYMBOLS_IN_LINE){
                int finalSymbol = getFinalSymbol(text);
                product_description_1.setText(text.substring(0, finalSymbol));
                if(finalSymbol < text.length()){
                    full_description.setVisibility(View.VISIBLE);
                    String s = (text.substring(finalSymbol)).trim();
                    product_description_2.setText(s);
                }
            }
            else {
                product_description_1.setText(text);
            }
            adapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.llVideo)
    public void showVideo(){
        Intent i = new Intent(getContext(), VideoActivity.class);
        i.putExtra("videoLink", videoLink);
        startActivity(i);
    }

    @OnClick(R.id.btnLeave)
    public void leave_request(){
        Intent sendRequest = new Intent(getContext(), SendRequestActivity.class);
        sendRequest.putExtra(Constants.PRODUCT_ID, productId);
        sendRequest.putExtra(Constants.PRODUCT_TITLE, productTitle);
        sendRequest.putExtra(Constants.MAIN_IMAGE_PATH, mainImagePath);
        startActivity(sendRequest);
    }


    private int getFinalSymbol(String s){
        int count = Constants.SYMBOLS_IN_LINE;
        while (count + 1 < s.length()){

            if(s.charAt(count)== '.'){
                return count + 1;
            }
            count++;
        }
        return s.length();
    }

    @Override
    public void showDialog(String s) {
        ((MainActivity) getActivity()).showDialog(s);
    }

    public void showNoConnectionDialog() {
        ((MainActivity) getActivity()).showNoConnectionDialog();
    }


    private void startShowInfo(ProductCharacteristic characteristic ){
    }
}
