
package com.example.olgarudnytska.sklad.data.response.station;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Station {

    @SerializedName("data")
    @Expose
    private StationData data;

    public StationData getData() {
        return data;
    }

    public void setData(StationData data) {
        this.data = data;
    }

}
