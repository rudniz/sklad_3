package com.example.olgarudnytska.sklad.presentation.interface_models;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public interface RecyclerListener {
    public void onClick(RecyclerView.ViewHolder holder, View view, int position);
}
