package com.example.olgarudnytska.sklad.presentation.view.sendRequest;

import com.example.olgarudnytska.sklad.presentation.base.IBaseView;

public interface ISendRequestView extends IBaseView  {
    void setResponse(String response);
    void showDialog(String s);
    void showNoConnectionDialog();
}
