package com.example.olgarudnytska.sklad.presentation.view.productionCategory;

import com.example.olgarudnytska.sklad.data.response.category_description.Category;
import com.example.olgarudnytska.sklad.presentation.base.IBaseView;

public interface IProductionCategoryView extends IBaseView {
    void setCategory(Category result);
    void showDialog(String s);
    void showNoConnectionDialog();

}

