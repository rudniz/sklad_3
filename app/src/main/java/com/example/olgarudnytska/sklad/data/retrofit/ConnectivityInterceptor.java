package com.example.olgarudnytska.sklad.data.retrofit;

import android.content.Context;
import android.util.Log;

import com.example.olgarudnytska.sklad.data.preferences.PreferencesTools;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ConnectivityInterceptor implements Interceptor {

    private PreferencesTools preferencesTools;

    public ConnectivityInterceptor(PreferencesTools preferencesTools) {
        this.preferencesTools = preferencesTools;
    }


    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!NetworkUtil.isOnline(preferencesTools.getContext())) {
            throw new NoConnectivityException();
        }

        Request.Builder builder = chain.request().newBuilder();
        return chain.proceed(builder.build());
    }


}
