package com.example.olgarudnytska.sklad.presentation.base;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.olgarudnytska.sklad.data.preferences.PreferencesTools;
import com.example.olgarudnytska.sklad.di.Injector;
import com.example.olgarudnytska.sklad.presentation.dialog.ProgressDialog;

import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseActivity extends AppCompatActivity implements IBaseView {

    private boolean isActive;
    private ProgressDialog progressDialog;
    private Unbinder unbinder;
    public int countProgressStart;
    protected PreferencesTools preferencesTools;
    private RelativeLayout dialogLayout;

    protected abstract int getLayoutId();
    protected abstract void initView();
    protected abstract void providePresenter();
    protected abstract void unbindPresenter();

    @Override
    protected void onStart() {
        super.onStart();
        isActive = true;
    }

    @Override
    protected void onStop() {
        isActive = false;
        super.onStop();
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        preferencesTools = Injector.getPreferences();
        if(!preferencesTools.getLanguage().equals(Locale.getDefault().getLanguage())) {
            setLocale(preferencesTools.getLanguage());
        }
        super.onCreate(savedInstanceState);
        isActive = true;
        countProgressStart = 0;
        setContentView(getLayoutId());
        providePresenter();
        unbinder = ButterKnife.bind(this);
        initView();

    }

    protected void setLocale(String l){
        Locale locale = new Locale(l);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

    }

    @Override
    public void showProgress() {
        if(progressDialog == null) {
            progressDialog = new ProgressDialog();
        }
        if (countProgressStart == 0) {
            if (isActive()) {
                progressDialog.show(getSupportFragmentManager(), ProgressDialog.TAG);
            }
        }
        countProgressStart++;
    }

    @Override
    public void hideProgress() {
        countProgressStart--;
        if (countProgressStart < 1) {
            countProgressStart = 0;
            if (isActive()) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        }
    }

    public void showDialog(){
        if(dialogLayout == null){
            dialogLayout = new RelativeLayout(this);
        }
        if(isActive()){
            dialogLayout.setVisibility(View.VISIBLE);
        }
    }

    public void hideDialog(){
        if(isActive()){
            dialogLayout.setVisibility(View.GONE);
        }
    }

// Прибирати клавіатуру при кліку за межами EditText
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_UP) {
            final View view = getCurrentFocus();
            if (view != null) {
                final boolean consumed = super.dispatchTouchEvent(ev);
                final View viewTmp = getCurrentFocus();
                final View viewNew = viewTmp != null ? viewTmp : view;
                if (viewNew.equals(view)) {
                    final Rect rect = new Rect();
                    final int[] coordinates = new int[2];
                    view.getLocationOnScreen(coordinates);
                    rect.set(coordinates[0], coordinates[1], coordinates[0] + view.getWidth(), coordinates[1] + view.getHeight());
                    final int x = (int) ev.getX();
                    final int y = (int) ev.getY();
                    if (rect.contains(x, y)) {
                        return consumed;
                    }
                } else if (viewNew instanceof EditText) {
                    return consumed;
                }
                final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(viewNew.getWindowToken(), 0);
                viewNew.clearFocus();
                return consumed;
            }
        }
        return super.dispatchTouchEvent(ev);
    }

}
