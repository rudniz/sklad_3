package com.example.olgarudnytska.sklad.presentation.view.production;

import com.example.olgarudnytska.sklad.data.response.Catalog;
import com.example.olgarudnytska.sklad.presentation.base.IBaseView;

import java.util.List;

public interface IProductionView extends IBaseView {
    void setCatalog(Catalog result);
    void showDialog(String s);
    void showNoConnectionDialog();

}
