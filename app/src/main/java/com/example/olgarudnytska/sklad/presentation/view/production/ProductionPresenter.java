package com.example.olgarudnytska.sklad.presentation.view.production;

import android.util.Log;

import com.example.olgarudnytska.sklad.data.repository.Repository;
import com.example.olgarudnytska.sklad.data.retrofit.NoConnectivityException;
import com.example.olgarudnytska.sklad.presentation.base.BasePresenter;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ProductionPresenter extends BasePresenter<IProductionView> {

    private Repository repository;

    public ProductionPresenter(Repository repository) {
        this.repository = repository;
    }

    public void loadCatalog() {
        Subscription subscription = repository.loadCatalog()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        view().setCatalog(list);
                    }
                }, throwable -> {
                    if(view().isActive()) {
                        view().hideProgress();
                        if(throwable instanceof NoConnectivityException){
                            view().showNoConnectionDialog();
                        }

                        else{
                            view().showDialog("loadProduct " + throwable.getMessage());
                        }
                    }
                });
        subscriptionsToUnbind.add(subscription);
    }
}
