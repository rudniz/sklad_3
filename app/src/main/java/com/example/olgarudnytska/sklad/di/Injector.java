package com.example.olgarudnytska.sklad.di;

import android.content.Context;
import android.util.Log;

import com.example.olgarudnytska.sklad.data.preferences.PreferencesTools;
import com.example.olgarudnytska.sklad.data.repository.RemoteStorage;
import com.example.olgarudnytska.sklad.data.repository.Repository;
import com.example.olgarudnytska.sklad.data.repository.ServerApi;
import com.example.olgarudnytska.sklad.data.retrofit.ServerApiFactory;

public class Injector {

    private static PreferencesTools preferences;
    private static Repository repository;
    private static RemoteStorage remoteStorage;
    private static ServerApi serverApi;

    public static void initInjector(Context context) {
        preferences = new PreferencesTools(context);
        serverApi = ServerApiFactory.getJsonApi(preferences);
        remoteStorage = new RemoteStorage(serverApi);
        repository = new Repository(preferences, remoteStorage);

    }

    public static Repository getRepository() {return repository;}
    public static PreferencesTools getPreferences() {return preferences;}
}
