package com.example.olgarudnytska.sklad.presentation.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.olgarudnytska.sklad.R;
import com.example.olgarudnytska.sklad.data.Language;
import com.example.olgarudnytska.sklad.presentation.interface_models.RecyclerListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Language> items;
    private RecyclerListener listener;

    public LanguageAdapter(List<Language> items, RecyclerListener listener) {
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LanguageHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_language_selection, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Language language = items.get(position);
        LanguageHolder lh = (LanguageHolder) holder;
        lh.languageTitle.setText(language.getLanguage());
        if(!language.isSelected()){
            lh.checkbox.setVisibility(View.INVISIBLE);
        }
        else{
            lh.checkbox.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class LanguageHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.language)
        TextView languageTitle;
        @BindView(R.id.checkImage)
        ImageView checkbox;

        public LanguageHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClick(LanguageHolder.this, v, getAdapterPosition());

                    }
                }
            });
        }
    }
}
